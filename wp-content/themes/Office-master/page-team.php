<?php  get_header(); ?>
    <?php if(have_posts()) {
        the_post(); 
        $page_thum = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full'); ?>
        <div class="row container-kamn">  
            <img src="<?php echo $page_thum[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
        </div>
    <?php } ?>


    <!-- Main Container -->

    <div id="banners"></div>
    <div class="container">
        <div class="row">
        <?php 
            $prefix= '_pref_';
            $team_posts = new WP_Query(array(
                'post_type'=>'team',
                'posts_per_page'=>-1,
                'order'=>'ASC',
                'team_cat'=>'developer',//This is slug name..
                'team_tag'=>'html+css', //This is slug name (+ use for more tags query)..
            ));
            if ($team_posts->have_posts()) {
                while($team_posts->have_posts()){
                    $team_posts->the_post();
                    $member_desig = get_post_meta(get_the_ID(),$prefix.'member_desig',true);
                    $block_color = get_post_meta(get_the_ID(),$prefix.'block_color',true);
                    $animate_cl = get_post_meta(get_the_ID(),$prefix.'animate_cl',true); ?>
                    <div class="col-md-6">
                        <div class="blockquote-box <?php echo $block_color;?> animated wow <?php echo $animate_cl;?> clearfix">
                            <div class="square pull-left">
                               <?php the_post_thumbnail('team_img_size');?>
                            </div>
                            <h4>
                                <?php the_title(); ?>
                            </h4>
                            <p>
                                <?php echo $member_desig;?>
                            </p>
                        </div>
                    </div>                    

            <?php }  }
                    else{ echo "No member add you.";}  ?>

        </div>
    </div>
    <!--End Main Container -->


 <?php get_footer(); ?>
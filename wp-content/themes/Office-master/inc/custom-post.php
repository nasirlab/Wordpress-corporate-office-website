<?php
//function custom field for slider 
function office_master_custom_post(){
	register_post_type('slider',array(
		'labels'=>array(
		'name'=>'Main Slider',
		'menu_name'=>'Slider menu',
		'all_items'=>'All Sliders',
		'add_new'=>'Add new slide',
		'add_new_item'=>'Add new slide item',			
		),
		'public'=>true,
		'supports'=>array(
			'title','thumbnail','revision','custom-fields','page-attributes'
		)

	));
		register_post_type('services',array(
		'labels'=>array(
		'name'=>'Services',
		'menu_name'=>'Service menu',
		'all_items'=>'All Services',
		'add_new'=>'Add new service',
		'add_new_item'=>'Add new service item',			
		),
		'public'=>true,
		'supports'=>array(
			'title','revision','custom-fields','page-attributes'
		)

	));
		

		register_post_type('team',array(
		'labels'=>array(
		'name'=>'Team',
		'menu_name'=>'Team member',
		'all_items'=>'Team members',
		'add_new'=>'Add new team member',
		'add_new_item'=>'Add new member',			
		),
		'public'=>true,
		'supports'=>array(
			'title','revision','page-attributes','thumbnail'
		)

	));

	// Function for team section category support(This function also usable out of this function)
		register_taxonomy('team_cat','team',array(
			'labels' => array(
				'name'=>'Team category',
				'menu_name'=>'Add category',
				'add_new_item'=>'Add new category',
			),
			'hierarchical'=> true,
			'show_admin_column'=> true,
		));

	// Function for team section tags support(This function also usable out of this function)
		register_taxonomy('team_tag','team',array(
			'labels' => array(
				'name'=>'Team tags',
				'menu_name'=>'Add tags',
				'add_new_item'=>'Add new tags',
			),
			'show_admin_column'=> true,
		));


}
add_action('init','office_master_custom_post');

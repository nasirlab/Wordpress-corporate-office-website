<?php 
/*
Template name:Template for contact page
*/ 
?>
<?php get_header(); ?>


        <!-- Main Container -->

        <div class="container-fluid-kamn">
            <div class="row">
                <div>
                    <iframe width="100%" height="450px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $redux_office['map_url']; ?>"></iframe>
                    <br />
                </div>
                <div class="col-lg-4 col-lg-offset-1">
                    <h4><?php echo $redux_office['con_add_title']; ?></h4>
                    <?php echo $redux_office['con_add_details']; ?>
                    <br>
                    <p class="lead"><?php echo $redux_office['con_soc_head']; ?></p><hr>
                    <?php  if (is_array($redux_office['con_soc_icon_details'])) {
                        foreach ($redux_office['con_soc_icon_details'] as $single_val) { ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <a href="<?php echo  $single_val['url']; ?>"><img src="<?php echo  $single_val['image']; ?>" alt="Skype"></a>
                            <?php echo  $single_val['title']; ?>
                        </div>
                     <?php   } } ?>
                    <br>
                </div>
                <div class="col-lg-5">
                    <div class="feedback-form">
          
                        <div id="contact-response"></div>
                        <?php echo do_shortcode('[contact-form-7 id="87" title="Our form"]'); ?>
                    </div> 
                </div>
            </div>
        </div>    
            
    <!--End Main Container -->


<?php get_footer(); ?>
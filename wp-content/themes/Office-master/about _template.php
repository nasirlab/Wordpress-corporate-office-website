<?php
/*
Template name: For about page
*/
  get_header(); ?>
    <?php
     $prefix= '_pref_';
     if(have_posts()) {
        the_post();
        $about_content = get_the_content();
        $group_data = get_post_meta(get_the_ID(), $prefix.'group_field' ,true); 
        $page_thum = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full'); ?>
        <div class="row container-kamn">  
            <img src="<?php echo $page_thum[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
        </div>
    <?php } ?>
    <!-- Main Container -->

    <div class="row container-kamn">
        <img src="assets/img/slider/slide1.jpg" class="blog-post" alt="Feature-img" align="right" width="100%"> 
    </div>

    <div id="banners"></div>
    <div class="container">   
        <div class="row">
            <div class="side-left col-sm-4 col-md-4">
            <?php foreach ($group_data as $single_data) { ?>
                <h3 class="lead"><?php echo $single_data[$prefix.'group_s_heding']; ?></h3><hr>
                <p><?php echo $single_data[$prefix.'group_s_desc']; ?></p>
                <?php 
                     // In this block achor title dynamic by two array comparing with index
                    if (is_array($single_data[$prefix.'group_s_link'])) {
                    foreach ($single_data[$prefix.'group_s_link'] as $array_index => $has_link) { ?>
                        <a href="<?php echo $has_link; ?>"><?php echo $single_data[$prefix.'group_s_link_title'][$array_index] ;?></a><br>
                <?php } } ?>
               <br>
            <?php } ?>
            </div>
            <div class="col-sm-8 col-md-8">
                <?php echo $about_content ;  ?>
            </div>  
        </div>    
    </div>  

    <!--End Main Container -->

<?php get_footer(); ?>
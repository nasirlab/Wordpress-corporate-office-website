    <!-- Begin #services-section -->
    <section id="services" class="services-section section-global-wrapper">
        <div class="container">
            <div class="row">
                <div class="services-header">
                    <h3 class="services-header-title">Our Mission</h3>
                    <p class="services-header-body"><em> Things we provide in Office </em>  </p><hr>
                </div>
            </div>
      
            <!-- Begin Services Row -->
            <div class="row services-row services-row-head services-row-1">
            <?php 
              $prefix= '_pref_';
               $service_posts = new WP_Query(array(
                  'post_type'=>'services',
                  'posts_per_page'=>-1,
                  'order'=>'ASC',
               ));
               if ($service_posts->have_posts()) {
                  while($service_posts->have_posts()){
                     $service_posts->the_post();
                     $service_icon = get_post_meta(get_the_ID(), $prefix.'service_icon',true);
                     $service_desc = get_post_meta(get_the_ID(), $prefix.'service_desc',true);
                     $service_btn_url = get_post_meta(get_the_ID(),$prefix.'service_btn_url',true);
                     $service_btn_name = get_post_meta(get_the_ID(),$prefix.'service_btn_name',true); 
                     $service_animate_cl = get_post_meta(get_the_ID(),$prefix.'service_animate_cl',true); ?>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <div class="services-group wow animated <?php echo $service_animate_cl ;?>" data-wow-offset="40">
                              <p class="services-icon"><span class="fa <?php echo $service_icon; ?> fa-5x"></span></p>
                              <h4 class="services-title"><?php the_title(); ?></h4>
                              <p class="services-body"><?php echo $service_desc; ?></p>
                              <p class="services-more"><a href="<?php echo $service_btn_url; ?>"><?php echo $service_btn_name; ?></a></p>
                          </div>
                      </div>

               <?php }  }else{ echo "Service is empty! no post found."; } ?>

            </div>
            <!-- End Serivces Row-->
    
        </div>      
    </section>
    <!-- End #services-section -->
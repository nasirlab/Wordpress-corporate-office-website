<?php get_header();
  $sorter_val = $redux_office['sorter_id']['Active'];
  if ($sorter_val) {
    foreach ($sorter_val as $key => $value) {
      switch($key){
        case 'slider':
             get_template_part('section-slider');
         break;        
         case 'service':
          get_template_part('section-service');
         break;        
         case 'red':
            get_template_part('section-red');
         break;        
         case 'green':
            get_template_part('section-green');
         break;        
         case 'blue':
            get_template_part('section-blue');
         break;
      }

    }
  }
get_footer(); ?>
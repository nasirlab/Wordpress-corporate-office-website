<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'office_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U`C[|8cM%$|BLls%qUT&Vdi:f#)/FnQ!)u,BhzmtWfy.2v8bLt}M*Ku {*US?HUF');
define('SECURE_AUTH_KEY',  ']/sKA3uqB}|IYiH2?+e%yC$E&8_9Vf}Ni]X5sLrCk<AdbO]X+T$sg2%X!#+RS)D0');
define('LOGGED_IN_KEY',    'S8iOpAh,B%ZlK<N8q_D3@Z_ !%iPM#K^ywD_1|p*N{*K!TIGCH8o==X+u$gRX)C6');
define('NONCE_KEY',        'R.ue])v?{P!nfxY`u1Xl|vDoD(o}M0;ryyg#bz,87Y>m[0>|K`[zg_yjAVyA$ts1');
define('AUTH_SALT',        '&L`;8DpC~sD2K</:C#%wg3M<.+e=H18w~>`3#R;7k8pjk-SU(Q*qCna:?]wsE%A<');
define('SECURE_AUTH_SALT', 'Rcn Hq;B$6b+HrMb` LkD]%|8YVkJ7:= {_mb[%kxF5+hJ8w,x;Ti4(71ds)f85.');
define('LOGGED_IN_SALT',   '-%{.d~V3k6{:7)NYL~AY<e?O 1_>RhD19dZuP[VI%*3>#n|=Wy nm7+:G/Vi7hzq');
define('NONCE_SALT',       '&3!nnD~=ji-Ft6`]TD >nf*!OqXc]r5!-i)(ktVN_A$Vg=e?=Q9drIK;p1P9^jOk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'off_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
